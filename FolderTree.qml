import QtQml.Models 2.3
import QtQuick 2.7
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.2
import QtQuick.Window 2.2

TreeView {
    id: treeView
    readonly property string currentFolder: p.currentFolder
    headerVisible: false
    frameVisible: false
    QtObject {
        id: p
        property string currentFolder
        function updateCurrentFolder(currentIndex) {
            // create the path by concatenting parent folder names
            var parent = model.parent(currentIndex);
            var path = model.name(currentIndex);
            var delimiter = model.delimiter(currentIndex);
            while (parent.valid) {
                path = model.name(parent) + delimiter + path;
                parent = model.parent(parent);
            }
            path = path.substr(2);
            p.currentFolder = path;
        }
    }
    TableViewColumn {
        title: "Name"
        role: "label"
        width: folders.width - 20
        delegate: Text {
            text: icon(styleData.value) + " " + styleData.value
            verticalAlignment: Text.AlignVCenter
            MouseArea {
                anchors.fill: parent
                onPressed: {
                    // TreeView reacts slow to a mouse click, so handle it here
                    p.updateCurrentFolder(styleData.index)
                    mouse.accepted = false
                }
            }
        }
    }
    style: TreeViewStyle {
        highlightedTextColor: palette.highlightedText
        backgroundColor: palette.base
        alternateBackgroundColor: palette.alternateBase
    }

    rowDelegate: Rectangle {
        height: 2.5 * systemFont.font.pixelSize
        color: styleData.selected
                   ? folders.activeFocus
                       ? palette.highlight
                       : Qt.lighter(palette.highlight, 1.5)
                   : rowMouse.containsMouse
                       ? Qt.lighter(palette.highlight, 1.8)
                       : palette.base
        MouseArea {
            id: rowMouse
            anchors.fill: parent
            hoverEnabled: true
            acceptedButtons: Qt.NoButton
        }
    }

    selectionMode: SelectionMode.SingleSelection
    selection: ItemSelectionModel {
        model: treeView.model
    }
    onCurrentIndexChanged: p.updateCurrentFolder(currentIndex)
    function setCurrentIndex(index) {
        selection.setCurrentIndex(index, ItemSelectionModel.Select);
        p.updateCurrentFolder(index);
    }
    function getFolderIndex(folder) {
        var parts = folder.split('/');
        var index = rootIndex;
        var i;
        for (i = 0; i < parts.length; ++i) {
            var rowCount = treeView.model.rowCount(index);
            var row;
            var newIndex;
            for (row = 0; row < rowCount; ++row) {
                var j = treeView.model.index(row, 0, index);
                if (treeView.model.name(j) === parts[i]) {
                    newIndex = j;
                }
            }
            if (!newIndex) {
                return;
            }
            index = newIndex;
        }
        return index;
    }
    function setCurrentFolder(folder) {
        setCurrentIndex(getFolderIndex(folder));
    }
}
