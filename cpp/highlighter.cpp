#include "highlighter.h"
#include <QtGui/QTextDocument>
#include <QtQuick/QQuickTextDocument>

namespace {
    QTextCharFormat greenFormat() {
        QTextCharFormat format;
        format.setForeground(Qt::darkGreen);
        return format;
    }
}

MailSyntaxHighlighter::MailSyntaxHighlighter() :QSyntaxHighlighter((QTextDocument*)0), pattern("^> .*$"), format(greenFormat()) {
}

void
MailSyntaxHighlighter::highlightBlock(const QString &text) {
    QRegularExpressionMatchIterator matchIterator = pattern.globalMatch(text);
    while (matchIterator.hasNext()) {
        QRegularExpressionMatch match = matchIterator.next();
        setFormat(match.capturedStart(), match.capturedLength(), format);
    }
}

EmailSyntaxHighlighter::EmailSyntaxHighlighter(QObject *parent)
    : QObject(parent) {
}

void EmailSyntaxHighlighter::setDocument(QQuickTextDocument* document) {
    m_highlighter.setDocument(document->textDocument());
}
