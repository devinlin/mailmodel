use crate::wait_until::wait_until;
use mailcore::Folder;
use mailcore::Observer;
use std::sync::{Arc, Condvar, Mutex};

pub struct FoldersReceiverData {
    pub done: bool,
    pub folders: Vec<Folder>,
    pub updates: usize,
}

#[derive(Clone)]
pub struct FoldersReceiver {
    data: Arc<(Mutex<FoldersReceiverData>, Condvar)>,
}

impl FoldersReceiver {
    pub fn set_done(&mut self, done: bool) {
        self.data.0.lock().unwrap().done = done;
    }
    pub fn wait_until(
        &self,
        f1: fn(&FoldersReceiverData) -> bool,
        check: fn(&FoldersReceiverData),
    ) {
        wait_until(&self.data, f1, check);
    }
}

impl FoldersReceiver {
    pub fn default() -> FoldersReceiver {
        FoldersReceiver {
            data: Arc::new((
                Mutex::new(FoldersReceiverData {
                    done: false,
                    folders: Vec::new(),
                    updates: 0,
                }),
                Condvar::new(),
            )),
        }
    }
}

impl Observer for FoldersReceiver {
    fn done(&self) -> bool {
        self.data.0.lock().unwrap().done
    }
}

impl mailcore::FoldersReceiver for FoldersReceiver {
    fn set_folders(&self, folders: Vec<Folder>) {
        let &(ref lock, ref cvar) = &*self.data;
        let mut data = lock.lock().unwrap();
        data.folders = folders;
        data.updates += 1;
        cvar.notify_one();
    }
}
