use crate::wait_until::wait_until;
use mailcore::message::Message;
use mailcore::sync_tree::{SyncedTree, TreeChange, TreeListener};
use mailcore::Observer;
use std::sync::{Arc, Condvar, Mutex, MutexGuard};

#[derive(Default)]
pub struct Null {}

impl TreeListener for Null {}

pub struct FolderReceiverData {
    pub done: bool,
    pub tree: SyncedTree<Arc<Message>, Null>,
    pub updates: usize,
}

#[derive(Clone)]
pub struct FolderReceiver {
    data: Arc<(Mutex<FolderReceiverData>, Condvar)>,
}

impl FolderReceiver {
    pub fn data(&self) -> MutexGuard<FolderReceiverData> {
        self.data.0.lock().unwrap()
    }
    pub fn set_done(&mut self, done: bool) {
        self.data.0.lock().unwrap().done = done;
    }
    pub fn wait_until(&self, f1: fn(&FolderReceiverData) -> bool, check: fn(&FolderReceiverData)) {
        wait_until(&self.data, f1, check);
    }
}

impl Default for FolderReceiver {
    fn default() -> FolderReceiver {
        FolderReceiver {
            data: Arc::new((
                Mutex::new(FolderReceiverData {
                    done: false,
                    tree: SyncedTree::default(),
                    updates: 0,
                }),
                Condvar::new(),
            )),
        }
    }
}

impl Observer for FolderReceiver {
    fn done(&self) -> bool {
        self.data.0.lock().unwrap().done
    }
}

impl mailcore::FolderReceiver for FolderReceiver {
    fn add_changes(&mut self, changes: Vec<TreeChange<Arc<Message>>>) {
        let &(ref lock, ref cvar) = &*self.data;
        let mut data = lock.lock().unwrap();
        for c in changes {
            data.tree.apply(c);
        }
        data.updates += 1;
        cvar.notify_one();
    }
}
