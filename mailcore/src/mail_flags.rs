use crate::gpg::find_subsequence;
use std::ffi::OsStr;
use std::os::unix::ffi::OsStrExt;

#[derive(Default, Debug, PartialEq, Clone)]
pub struct MailFlags {
    pub draft: bool,
    pub flagged: bool,
    pub passed: bool,
    pub replied: bool,
    pub seen: bool,
    pub trashed: bool,
}

impl MailFlags {
    pub fn from_file_name<S: AsRef<OsStr> + ?Sized>(file_name: &S) -> Option<MailFlags> {
        let bytes = file_name.as_ref().as_bytes();
        if let Some(pos) = find_subsequence(bytes, b":2,") {
            let mut iter = bytes.iter().skip(pos + 3).cloned();
            let mut flags = MailFlags::default();
            let mut c = iter.next();
            if c == Some(b'D') {
                flags.draft = true;
                c = iter.next();
            }
            if c == Some(b'F') {
                flags.flagged = true;
                c = iter.next();
            }
            if c == Some(b'P') {
                flags.passed = true;
                c = iter.next();
            }
            if c == Some(b'R') {
                flags.replied = true;
                c = iter.next();
            }
            if c == Some(b'S') {
                flags.seen = true;
                c = iter.next();
            }
            flags.trashed = c == Some(b'T');
            Some(flags)
        } else {
            None
        }
    }
    pub fn from_seen(seen: bool) -> MailFlags {
        MailFlags {
            seen,
            ..MailFlags::default()
        }
    }
}

#[test]
fn test_from_file_name() {
    assert_eq!(MailFlags::from_file_name(":1,"), None);
    assert_eq!(MailFlags::from_file_name(":2,"), Some(MailFlags::default()));
    assert_eq!(
        MailFlags::from_file_name(":2,S"),
        Some(MailFlags {
            seen: true,
            ..Default::default()
        })
    );
    assert_eq!(
        MailFlags::from_file_name(":2,DFPRST"),
        Some(MailFlags {
            draft: true,
            flagged: true,
            passed: true,
            replied: true,
            seen: true,
            trashed: true,
            ..Default::default()
        })
    );
}
