use crate::message::{get_header_or_none, Message, Uid};
use lettre::SendableEmail;
use lettre_email::EmailBuilder;
use mailparse::{parse_headers, MailHeader, MailHeaderMap};

#[derive(Clone, Copy)]
pub enum RecipientType {
    To,
    CC,
    BCC,
}

impl Default for RecipientType {
    fn default() -> RecipientType {
        RecipientType::To
    }
}

impl From<&str> for RecipientType {
    fn from(rt: &str) -> RecipientType {
        if rt == "CC" {
            RecipientType::CC
        } else if rt == "BCC" {
            RecipientType::BCC
        } else {
            RecipientType::To
        }
    }
}

impl From<RecipientType> for &str {
    fn from(rt: RecipientType) -> &'static str {
        match rt {
            RecipientType::To => "To",
            RecipientType::CC => "CC",
            RecipientType::BCC => "BCC",
        }
    }
}

#[derive(Default)]
pub struct Recipient {
    pub to_cc_or_bcc: RecipientType,
    pub address: String,
}

#[derive(Default)]
pub struct NewMessage {
    pub body: String,
    pub subject: String,
    pub in_reply_to: Option<String>,
    pub references: Option<String>,
    pub recipients: Vec<Recipient>,
}

impl NewMessage {
    fn init_reply(bytes: &[u8]) -> (Vec<MailHeader>, NewMessage) {
        let email = Message::from_bytes(Uid(0), None, bytes.into(), None).unwrap();
        let (headers, _) = parse_headers(bytes).unwrap();
        let subject = &email.subject;
        let subject = if subject.starts_with("Re: ") {
            subject.clone()
        } else {
            let mut s = String::from("Re: ");
            s.push_str(subject);
            s
        };
        let in_reply_to = Some(email.message_id.clone());
        let references = get_header_or_none(&headers, "References");
        let references = Some(if let Some(mut references) = references {
            references.push(' ');
            references.push_str(&email.message_id);
            references
        } else {
            email.message_id.clone()
        });
        let body = NewMessage::get_reply_body(&email);
        (
            headers,
            NewMessage {
                body,
                in_reply_to,
                recipients: Vec::new(),
                references,
                subject,
            },
        )
    }
    fn get_reply_body(email: &Message) -> String {
        let body = email.get_alternative("text/plain");
        let mut reply_body = String::from("You wrote:\n> ");
        reply_body.push_str(&body.replace("\n", "\n> "));
        reply_body
    }
    pub fn bytes(&self) -> Vec<u8> {
        let mut builder = EmailBuilder::new()
            .from("jos@vandenoever.info")
            .subject(self.subject.clone())
            .body(self.body.clone());
        for recipient in &self.recipients {
            match recipient.to_cc_or_bcc {
                RecipientType::To => {
                    builder = builder.to(recipient.address.clone());
                }
                RecipientType::CC => {
                    builder = builder.cc(recipient.address.clone());
                }
                RecipientType::BCC => {
                    builder = builder.bcc(recipient.address.clone());
                }
            }
        }
        if let Some(in_reply_to) = &self.in_reply_to {
            builder = builder.in_reply_to(in_reply_to.clone());
        }
        if let Some(references) = &self.references {
            builder = builder.header(("References", references.clone()));
        }
        let email = builder.build().expect("Failed to build message");
        let email: SendableEmail = email.into();
        let email_string = email
            .message_to_string()
            .expect("Failed to convert to string");
        eprintln!("{}", email_string);
        email_string.into_bytes()
    }
    pub fn reply_all_from_mail_bytes(bytes: &[u8]) -> NewMessage {
        // get the subject, recipients, and body
        let (headers, mut msg) = NewMessage::init_reply(bytes);
        if msg.add_recipients(&headers, "Reply-To", RecipientType::To) {
            msg.add_recipients(&headers, "From", RecipientType::CC);
        } else {
            msg.add_recipients(&headers, "From", RecipientType::To);
        }
        msg.add_recipients(&headers, "CC", RecipientType::CC);
        msg.add_recipients(&headers, "BCC", RecipientType::BCC);
        msg
    }
    pub fn reply_from_mail_bytes(bytes: &[u8]) -> NewMessage {
        // get the subject, recipients, and body
        let (headers, mut msg) = NewMessage::init_reply(bytes);
        if !msg.add_recipients(&headers, "Reply-To", RecipientType::To) {
            msg.add_recipients(&headers, "From", RecipientType::To);
        }
        msg
    }
    fn add_recipients(
        &mut self,
        headers: &[MailHeader],
        header: &str,
        to_cc_or_bcc: RecipientType,
    ) -> bool {
        let mut added = false;
        let addresses = headers.get_all_values(header);
        for address in addresses
            .iter()
            .flat_map(|a| a.split(','))
            .map(|a| a.trim())
        {
            self.recipients.push(Recipient {
                to_cc_or_bcc,
                address: address.trim().to_string(),
            });
            added = true;
        }
        added
    }
}
