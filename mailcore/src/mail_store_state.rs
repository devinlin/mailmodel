use crate::message_threads::MessageThreads;
use std::collections::BTreeMap;
use std::sync::Arc;

#[derive(Default, Clone)]
pub struct MailFolder {
    pub threads: Arc<MessageThreads>,
}

impl MailFolder {
    pub fn new(threads: Arc<MessageThreads>) -> MailFolder {
        MailFolder { threads }
    }
}

#[derive(Default)]
pub struct MailStoreState {
    pub folders: BTreeMap<String, Arc<MailFolder>>,
}

impl MailStoreState {
    pub fn get_folder(&self, name: &str) -> Option<Arc<MailFolder>> {
        self.folders.get(name).map(|f| Arc::clone(f))
    }
    pub fn set_folder(&mut self, name: String, folder: Arc<MailFolder>) {
        self.folders.insert(name, folder);
    }
    pub fn clear(&mut self) {
        self.folders.clear();
    }
}
