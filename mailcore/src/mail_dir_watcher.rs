use crate::db;
use crate::errors::{ErrorKind, Result, ResultExt};
use crate::indexed_mail_dir_store::IndexedMailDirConfig;
use crate::kmail_dir;
use crate::mail_dir_index::{Db, MailFileRow};
use crate::mail_flags::MailFlags;
use crate::mail_store::{EventHandler, MailStoreChange, Name};
use crate::message::{Message, Uid};
use notify::{DebouncedEvent, RecommendedWatcher, RecursiveMode, Watcher};
use rayon::prelude::*;
use rusqlite::Connection;
use sha2::{digest::generic_array::GenericArray, Digest, Sha256};
use std::cmp::Ordering;
use std::collections::{BTreeMap, HashSet};
use std::fs;
use std::path::{Path, PathBuf};
use std::sync::mpsc::channel;
use std::sync::{Arc, Mutex};
use std::thread;
use std::time::{Duration, SystemTime};
use typenum::consts::U32;
use walkdir::WalkDir;

pub struct MailDirWatcher {
    shared: Arc<Mutex<Shared>>,
    handle: Option<thread::JoinHandle<()>>,
}

impl MailDirWatcher {
    #[allow(clippy::new_ret_no_self)]
    pub fn new<H: EventHandler>(
        config: IndexedMailDirConfig,
        event_sink: Arc<Mutex<H>>,
    ) -> Result<Self> {
        let shared = Arc::new(Mutex::new(Shared {
            folders_to_update: Vec::new(),
            stop: false,
            watcher: None,
        }));
        let s = shared.clone();
        let connection = db::open(&config.index_file)?;
        let handle = thread::spawn(move || loop {
            match run(&s, &config, &connection, &event_sink) {
                Err(e) => match e.kind() {
                    ErrorKind::WatcherStopped => break,
                    _ => eprintln!("Update thread crashed: {:?}", e),
                },
                Ok(false) => {
                    break;
                }
                _ => {}
            }
        });
        Ok(MailDirWatcher {
            shared,
            handle: Some(handle),
        })
    }
    fn stop(&mut self) {
        if let Ok(mut shared) = self.shared.lock() {
            shared.stop = true;
            // cause the watcher receiver to stop
            eprintln!("hanging up watcher");
            shared.watcher = None;
        }
    }
}

impl Drop for MailDirWatcher {
    fn drop(&mut self) {
        self.stop();
        if let Some(handle) = self.handle.take() {
            if let Err(e) = handle.join() {
                eprintln!("{:?}", e);
            }
            eprintln!("THREAD DONE!");
        }
    }
}

struct Shared {
    folders_to_update: Vec<kmail_dir::MailDir>,
    watcher: Option<RecommendedWatcher>,
    stop: bool,
}

struct Sender<'a, H: EventHandler> {
    shared: &'a Arc<Mutex<Shared>>,
    event_sink: &'a Arc<Mutex<H>>,
}

impl<'a, H: EventHandler> Sender<'a, H> {
    fn handle_event(&self, c: MailStoreChange) -> Result<()> {
        if self.shared.lock().unwrap().stop {
            return Err(ErrorKind::WatcherStopped.into());
        }
        self.event_sink.lock().unwrap().handle_event(c);
        Ok(())
    }
}

fn run<H: EventHandler>(
    shared: &Arc<Mutex<Shared>>,
    config: &IndexedMailDirConfig,
    connection: &Connection,
    event_sink: &Arc<Mutex<H>>,
) -> Result<bool> {
    let (tx, receiver) = channel();
    // wait 50 ms seconds before sending events
    let mut watcher: RecommendedWatcher = Watcher::new(tx, Duration::from_millis(50))?;
    watcher
        .watch(&config.mail_dir, RecursiveMode::Recursive)
        .chain_err(|| ErrorKind::FileNotFound(config.mail_dir.to_path_buf()))?;
    let folders_to_update = kmail_dir::get_mail_dirs(&config.mail_dir);
    //    while folders_to_update.len() > 3 {
    //        folders_to_update.pop();
    //    }
    let folders = folders_to_update.clone();
    if let Ok(mut shared) = shared.lock() {
        shared.watcher = Some(watcher);
        shared.folders_to_update = folders_to_update;
    } else {
        return Ok(false);
    }
    let mut db = Db::new(connection)?;
    let (folder_map, _) = sync_folder_list(&mut db, &folders)?;
    let event_sink = Sender { shared, event_sink };
    event_sink.handle_event(MailStoreChange::FoldersListChanged)?;
    // update the folder contents
    loop {
        if done(&shared) {
            break;
        }
        // get the next folder to work on
        let dir = shared.lock().unwrap().folders_to_update.pop();
        if let Some(dir) = dir {
            if let Some((folder_id, folder)) = folder_map.iter().find(|d| *d.1 == dir) {
                let folder = config.mail_dir.join(&folder.path);
                dir_index(&mut db, *folder_id, &folder, dir.name, &event_sink);
            }
        } else {
            break;
        }
    }
    db.clean_up_mails_and_blobs()?;
    let mut to_add = Vec::new();
    let mut to_delete = Vec::new();
    let notify_delay = Duration::from_millis(50);
    loop {
        if done(&shared) {
            return Ok(false);
        }

        // ideally this would use receiver.recv(), but dropping the watcher does
        // not hang up the sender
        while let Ok(event) = receiver.recv_timeout(notify_delay) {
            eprintln!("{:?}", event);
            match event {
                DebouncedEvent::NoticeWrite(_path) => {}
                DebouncedEvent::NoticeRemove(_path) => {}
                DebouncedEvent::Create(path) => {
                    to_add.push(path);
                }
                DebouncedEvent::Write(_path) => {}
                DebouncedEvent::Chmod(_path) => {}
                DebouncedEvent::Remove(path) => to_delete.push(path),
                DebouncedEvent::Rename(a, b) => {
                    to_delete.push(a);
                    to_add.push(b);
                }
                DebouncedEvent::Rescan => {
                    return Ok(true);
                }
                DebouncedEvent::Error(error, path) => {
                    if let Some(path) = path {
                        eprintln!("{}: {}", error, path.display());
                    } else {
                        eprintln!("{}", error);
                    }
                }
            }
            if done(&shared) {
                return Ok(false);
            }
        }
        let mut changed_folders = HashSet::new();
        if delete(
            &mut db,
            &config.mail_dir,
            &mut to_delete,
            &folder_map,
            &mut changed_folders,
        )? {
            // a folder was deleted, do full rescan
            return Ok(true);
        }
        if add(
            &mut db,
            &config.mail_dir,
            &mut to_add,
            &folder_map,
            &mut changed_folders,
        ) {
            // got a new folder, do full rescan
            return Ok(true);
        }
        for folder_name in changed_folders {
            event_sink.handle_event(MailStoreChange::FolderChanged(Name {
                name: folder_name,
                delimiter: "/".into(),
            }))?;
        }
        to_add.clear();
        to_delete.clear();
    }
}

fn done(shared: &Arc<Mutex<Shared>>) -> bool {
    if let Ok(shared) = shared.lock() {
        shared.stop
    } else {
        true
    }
}

fn delete(
    db: &mut Db,
    mail_dir: &Path,
    to_delete: &mut Vec<PathBuf>,
    folders: &BTreeMap<i64, kmail_dir::MailDir>,
    changed_folders: &mut HashSet<String>,
) -> Result<bool> {
    for to_delete in to_delete {
        let parent = to_delete.parent();
        if let (Some(Ok(mail_folder)), Some(parent), Some(file_name)) = (
            parent
                .and_then(|d| d.parent())
                .map(|d| d.strip_prefix(mail_dir)),
            parent.and_then(|d| d.file_name()).and_then(|d| d.to_str()),
            to_delete.file_name().and_then(|f| f.to_str()),
        ) {
            if parent == "new" || parent == "cur" {
                if let Some(mail_folder) = folders.iter().find(|f| f.1.path == mail_folder) {
                    let seen = parent == "cur";
                    db.delete_mail_file_by_path(*mail_folder.0, seen, file_name)?;
                    changed_folders.insert(mail_folder.1.name.clone());
                }
            }
        } else if let Ok(to_delete) = to_delete.strip_prefix(mail_dir) {
            if folders.iter().any(|f| f.1.path == to_delete) {
                // a whole folder was deleted, order a rescan
                return Ok(true);
            }
        }
    }
    Ok(false)
}

fn add(
    db: &mut Db,
    mail_dir: &Path,
    to_add: &mut Vec<PathBuf>,
    folders: &BTreeMap<i64, kmail_dir::MailDir>,
    changed_folders: &mut HashSet<String>,
) -> bool {
    let mut map = BTreeMap::new();
    for to_add in to_add {
        if let Some(file) = metadata(to_add) {
            if let Some(Ok(mail_folder)) = to_add
                .parent()
                .and_then(|d| d.parent())
                .map(|d| d.strip_prefix(mail_dir))
            {
                if let Some(mail_folder) = folders.iter().find(|f| f.1.path == mail_folder) {
                    map.entry(*mail_folder.0)
                        .or_insert_with(|| {
                            let folder = mail_dir.join(&mail_folder.1.path);
                            (folder, Vec::new())
                        })
                        .1
                        .push(file);
                    changed_folders.insert(mail_folder.1.name.clone());
                }
            }
        } else if let Ok(metadata) = to_add.metadata() {
            if metadata.is_dir() {
                // a directory was added, too hard, just do a rescan
                return true;
            }
        }
    }
    for m in map {
        add_mails(db, m.0, &(m.1).0, (m.1).1);
    }
    false
}

fn sync_folder_list(
    db: &mut Db,
    folders: &[kmail_dir::MailDir],
) -> Result<(BTreeMap<i64, kmail_dir::MailDir>, bool)> {
    // get the list of folders from the database
    let db_folders = db.list_mail_folders()?;
    let mut folder_map = BTreeMap::<i64, kmail_dir::MailDir>::new();
    let mut change = false;
    // remove outdated folders from database
    for db_folder in &db_folders {
        if !folders
            .iter()
            .any(|f| f.name == db_folder.1.name || f.path == db_folder.1.path)
        {
            db.delete_mail_folder(*db_folder.0)?;
            change = true;
        } else {
            folder_map.insert(*db_folder.0, db_folder.1.clone());
        }
        eprintln!("db folder '{}'", db_folder.1.name);
    }
    // add missing folders to database
    eprintln!("current folders {}", folders.len());
    for folder in folders {
        eprintln!("folder '{}'", folder.name);
        if !db_folders
            .iter()
            .any(|f| f.1.name == folder.name || f.1.path == folder.path)
        {
            let id = db.insert_mail_folder(&folder.path, &folder.name)?;
            folder_map.insert(id, folder.clone());
            change = true;
        }
    }
    Ok((folder_map, change))
}

// updating the index
// loop over all files with WalkDir
// WalkDir can sort files by name
// so to compare with the database, we should
//
fn dir_index<H: EventHandler>(
    db: &mut Db,
    folder_id: i64,
    folder: &Path,
    folder_name: String,
    event_sink: &Sender<H>,
) {
    // loop over files that are likely to be mails and get the size and mtime
    // for each
    let mails = list_mails_thread(&folder);
    let _ = compare_mails(db, folder_name, folder_id, folder, mails, event_sink);
}

/// List mail files alphabetically.
/// Files in the 'cur' folder will be listed before files in the 'new' folder.
fn list_mails(folder_path: &Path) -> impl Iterator<Item = MailFileRow> {
    WalkDir::new(&folder_path)
        .sort_by(|a, b| a.path().cmp(b.path()))
        .into_iter()
        .filter_entry(|e| {
            if e.depth() == 0 {
                true
            } else if e.depth() == 1 && e.file_type().is_dir() {
                let dir_name = e.path().file_name().unwrap();
                dir_name == "cur" || dir_name == "new" || dir_name == "tmp"
            } else {
                e.depth() == 2 && e.file_type().is_file()
            }
        })
        .filter_map(|e| e.ok())
        .filter_map(|e| metadata(e.path()))
}

fn list_mails_thread(folder_path: &Path) -> impl Iterator<Item = MailFileRow> {
    let path_buf = folder_path.to_path_buf();
    let (tx, rx) = channel();
    thread::spawn(move || {
        for mail in list_mails(&path_buf) {
            if tx.send(mail).is_err() {
                // stop if sending fails
                break;
            }
        }
    });
    rx.into_iter()
}

/// create a MailFileRow if the path points to a file, has a parent that is
/// 'new' or 'cur'
fn metadata(path: &Path) -> Option<MailFileRow> {
    if let (Ok(metadata), Some(file_name), Some(parent)) = (
        path.metadata(),
        path.file_name().and_then(|f| f.to_str()),
        path.parent()
            .and_then(|p| p.file_name())
            .and_then(|p| p.to_str()),
    ) {
        if !metadata.file_type().is_file() || (parent != "cur" && parent != "new") {
            return None;
        }
        if let Ok(mtime) = metadata.modified() {
            if let Ok(mtime) = mtime.duration_since(SystemTime::UNIX_EPOCH) {
                let mtime = mtime.as_secs();
                let flags = MailFlags::from_file_name(file_name);
                let new = parent == "new";
                let seen = if let Some(flags) = flags {
                    flags.seen
                } else {
                    !new
                };
                return Some(MailFileRow {
                    id: 0,
                    file_name: file_name.to_string(),
                    seen,
                    new,
                    mtime,
                    length: metadata.len(),
                });
            }
        }
    }
    None
}

fn cmp_mail_file(a: &MailFileRow, b: &MailFileRow) -> Ordering {
    if a.new != b.new {
        return a.new.cmp(&b.new);
    }
    a.file_name.cmp(&b.file_name)
}

fn compare_mails<H: EventHandler>(
    db: &mut Db,
    folder_name: String,
    folder_id: i64,
    dir: &Path,
    mut mail_iterator: impl Iterator<Item = MailFileRow>,
    event_sink: &Sender<H>,
) -> Result<()> {
    let mut rows = db.list_mail_files(folder_id)?;
    let mut to_delete = Vec::new();
    let mut to_add = Vec::new();
    let mut row = rows.next();
    let mut mail = mail_iterator.next();
    loop {
        match (mail.take(), row.take()) {
            (Some(m), Some(r)) => {
                match cmp_mail_file(&m, &r) {
                    Ordering::Equal => {
                        // if mtime or size changed, the mail is re-added
                        if m.mtime != r.mtime || m.length != r.length {
                            to_delete.push(r.id);
                            to_add.push(m);
                        }
                        mail = mail_iterator.next();
                        row = rows.next();
                    }
                    Ordering::Less => {
                        to_add.push(m);
                        mail = mail_iterator.next();
                        row = Some(r);
                    }
                    Ordering::Greater => {
                        to_delete.push(r.id);
                        row = rows.next();
                        mail = Some(m);
                    }
                }
            }
            (Some(m), None) => {
                to_add.push(m);
                mail = mail_iterator.next();
            }
            (None, Some(r)) => {
                to_delete.push(r.id);
                row = rows.next();
            }
            (None, None) => break,
        }
    }
    drop(rows);
    let has_change = !to_add.is_empty() || !to_delete.is_empty();
    if has_change {
        eprintln!(
            "{} adding {} removing {}",
            folder_name,
            to_add.len(),
            to_delete.len()
        );
    }
    db.delete_mails(to_delete)?;
    add_mails(db, folder_id, dir, to_add);
    if has_change {
        event_sink.handle_event(MailStoreChange::FoldersListChanged)?;
        event_sink.handle_event(MailStoreChange::FolderChanged(Name {
            name: folder_name,
            delimiter: "/".into(),
        }))?;
    }
    Ok(())
}

fn add_mails(db: &mut Db, folder_id: i64, dir: &Path, to_add: Vec<MailFileRow>) {
    // calculate the sha256 for all mails
    let sha256s: Vec<_> = to_add
        .into_par_iter()
        .filter_map(|mail| {
            let path = if mail.new {
                dir.join("new").join(&mail.file_name)
            } else {
                dir.join("cur").join(&mail.file_name)
            };
            if let Ok(bytes) = fs::read(&path) {
                let sha256 = hash(&bytes);
                let flags = MailFlags::from_file_name(path.file_name().unwrap());
                let message = Message::from_header_bytes(Uid(0), flags, &bytes, Some(path)).ok();
                Some((mail, sha256, message))
            } else {
                None
            }
        })
        .collect();
    for (mail, sha256, message) in sha256s {
        if let Err(e) = db.add_mail(&mail, sha256, folder_id, message) {
            eprintln!("{:?}", e);
        }
    }
}

fn hash(bytes: &[u8]) -> GenericArray<u8, U32> {
    let mut hasher = Sha256::default();
    hasher.update(bytes);
    hasher.finalize()
}
