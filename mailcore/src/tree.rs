use std::cmp::Ordering;
use std::ops::{Index, IndexMut};

struct Node<T> {
    data: T,
    children: Vec<usize>,
    parent: Option<usize>,
}

pub struct Tree<T> {
    nodes: Vec<Node<T>>,
    roots: Vec<usize>,
}

impl<T> Tree<T> {
    pub fn clear(&mut self) {
        self.nodes.clear();
        self.roots.clear();
    }
    pub fn len(&self) -> usize {
        self.nodes.len()
    }
    fn add(&mut self, parent: Option<usize>, t: T) -> (usize, &mut Vec<usize>) {
        let index = self.nodes.len();
        self.nodes.push(Node {
            data: t,
            children: Vec::new(),
            parent,
        });
        (
            index,
            if let Some(parent) = parent {
                &mut self.nodes[parent].children
            } else {
                &mut self.roots
            },
        )
    }
    pub fn insert(&mut self, parent: Option<usize>, row: usize, t: T) -> usize {
        let r = self.add(parent, t);
        r.1.insert(row, r.0);
        r.0
    }
    #[allow(dead_code)]
    pub fn prepend(&mut self, parent: Option<usize>, t: T) -> usize {
        self.insert(parent, 0, t)
    }
    pub fn append(&mut self, parent: Option<usize>, t: T) -> usize {
        let r = self.add(parent, t);
        r.1.push(r.0);
        r.0
    }
    fn children(&self, index: Option<usize>) -> &Vec<usize> {
        if let Some(index) = index {
            &self.nodes[index].children
        } else {
            &self.roots
        }
    }
    pub fn row_count(&self, index: Option<usize>) -> usize {
        self.children(index).len()
    }
    pub fn index(&self, index: Option<usize>, row: usize) -> usize {
        self.children(index)[row]
    }
    pub fn parent(&self, index: usize) -> Option<usize> {
        self.nodes[index].parent
    }
    pub fn row(&self, index: usize) -> usize {
        self.children(self.nodes[index].parent)
            .iter()
            .position(|i| *i == index)
            .unwrap()
    }
    pub fn check_row(&self, index: usize, row: usize) -> Option<usize> {
        if index < self.nodes.len() {
            let parent = self.nodes[index].parent;
            if row < self.row_count(parent) && self.index(parent, row) == index {
                Some(row)
            } else {
                Some(self.row(index))
            }
        } else {
            None
        }
    }
    pub fn depth(&self, index: usize) -> usize {
        let mut parent = self.parent(index);
        let mut depth = 0;
        while let Some(p) = parent {
            depth += 1;
            parent = self.parent(p);
        }
        depth
    }
    pub fn binary_search_by<'a, F>(&'a self, index: Option<usize>, f: F) -> Result<usize, usize>
    where
        F: FnMut(&'a usize) -> Ordering,
    {
        self.children(index).binary_search_by(f)
    }
}

impl<T> Default for Tree<T> {
    fn default() -> Tree<T> {
        Tree {
            nodes: Vec::new(),
            roots: Vec::new(),
        }
    }
}

impl<T> Index<usize> for Tree<T> {
    type Output = T;
    fn index(&self, index: usize) -> &T {
        &self.nodes[index].data
    }
}

impl<T> IndexMut<usize> for Tree<T> {
    fn index_mut(&mut self, index: usize) -> &mut T {
        &mut self.nodes[index].data
    }
}

impl<T> Clone for Node<T>
where
    T: Clone,
{
    fn clone(&self) -> Node<T> {
        Node {
            data: self.data.clone(),
            children: self.children.clone(),
            parent: self.parent,
        }
    }
}

impl<T> PartialEq for Node<T>
where
    T: PartialEq,
{
    fn eq(&self, o: &Node<T>) -> bool {
        self.data.eq(&o.data) && self.children.eq(&o.children) && self.parent.eq(&o.parent)
    }
}

impl<T> Clone for Tree<T>
where
    T: Clone,
{
    fn clone(&self) -> Tree<T> {
        Tree {
            nodes: self.nodes.clone(),
            roots: self.roots.clone(),
        }
    }
}

impl<T> PartialEq for Tree<T>
where
    T: PartialEq,
{
    fn eq(&self, o: &Tree<T>) -> bool {
        self.nodes.eq(&o.nodes) && self.roots.eq(&o.roots)
    }
}
