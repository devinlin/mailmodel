use crate::errors::Result;
use crate::mail_flags::MailFlags;
use chrono::offset::FixedOffset;
use chrono::DateTime;
use mailparse::{parse_headers, parse_mail, MailHeader, MailHeaderMap, ParsedMail};
use std::cmp::Ordering;
use std::path::PathBuf;

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Copy, Debug)]
pub struct Uid(pub usize);

#[derive(Clone, Debug)]
pub struct Message {
    pub uid: Uid,
    pub subject: String,
    pub from: String,
    pub message_id: String,
    pub in_reply_to: Option<String>,
    pub flags: Option<MailFlags>,
    pub date: DateTime<FixedOffset>,
    pub thread_end: DateTime<FixedOffset>,
    pub data: Vec<u8>,
    pub file_path: Option<PathBuf>,
}

impl Default for Message {
    fn default() -> Message {
        let dummy = DateTime::parse_from_rfc2822("Tue, 1 Jul 2003 10:52:37 +0200").unwrap();
        Message {
            uid: Uid(0),
            subject: String::new(),
            from: String::new(),
            message_id: String::new(),
            in_reply_to: None,
            flags: Some(MailFlags::default()),
            date: dummy,
            thread_end: dummy,
            data: Vec::new(),
            file_path: None,
        }
    }
}

impl PartialEq for Message {
    fn eq(&self, other: &Message) -> bool {
        self.uid.eq(&other.uid) && self.flags.eq(&other.flags)
    }
}

impl Eq for Message {}

impl PartialOrd for Message {
    fn partial_cmp(&self, other: &Message) -> Option<Ordering> {
        Some(self.uid.cmp(&other.uid))
    }
}

impl Ord for Message {
    fn cmp(&self, other: &Self) -> Ordering {
        self.uid.cmp(&other.uid)
    }
}

fn parse_date(date: &str) -> Result<DateTime<FixedOffset>> {
    let r = if let Some(pos) = date.find(" -0000") {
        DateTime::parse_from_rfc2822(&format!("{} +0000", &date[..pos]))
    } else if let Some(pos) = date.find(" (") {
        DateTime::parse_from_rfc2822(&date[..pos])
    } else {
        DateTime::parse_from_rfc2822(&date)
    };
    match r {
        Err(e) => {
            println!("could not parse date '{}'", date);
            Err(e)?
        }
        Ok(o) => Ok(o),
    }
}

fn date_from_received(date: &str) -> Option<DateTime<FixedOffset>> {
    if let Some(pos) = date.find(">; ") {
        if let Some(pos2) = date[pos + 3..].find(" (") {
            let date = &date[pos + 3..pos + 3 + pos2];
            if let Ok(date) = DateTime::parse_from_rfc2822(date) {
                return Some(date);
            }
        }
    }
    None
}

fn get_date_from_received(headers: &[MailHeader]) -> Result<DateTime<FixedOffset>> {
    let mut date = None;
    for header in headers {
        let key = header.get_key();
        if key.eq_ignore_ascii_case("received") {
            let value = header.get_value();
            let d = date_from_received(&value);
            if d.is_some() && (date.is_none() || d < date) {
                date = d;
            }
        }
    }
    date.ok_or_else(|| "Could not find a date in the Received headers.".into())
}

impl Message {
    fn headers_to_message(
        uid: Uid,
        flags: Option<MailFlags>,
        headers: &[MailHeader],
        file_path: Option<PathBuf>,
    ) -> Result<Message> {
        let date = get_header_or_empty(headers, "Date");
        let date = date.trim();
        let date = if date.is_empty() {
            get_date_from_received(headers)?
        } else {
            parse_date(&date)?
        };
        Ok(Message {
            uid,
            subject: get_header_or_empty(headers, "Subject"),
            from: get_header_or_empty(headers, "From"),
            message_id: get_header_or_empty(headers, "Message-ID"),
            in_reply_to: get_header_or_none(headers, "In-Reply-To"),
            flags,
            date,
            thread_end: date,
            data: Vec::new(),
            file_path,
        })
    }
    pub fn from_header_bytes(
        uid: Uid,
        flags: Option<MailFlags>,
        data: &[u8],
        file_path: Option<PathBuf>,
    ) -> Result<Message> {
        let headers = parse_headers(&data)?.0;
        let message = Message::headers_to_message(uid, flags, &headers, file_path)?;
        Ok(message)
    }
    pub fn from_bytes(
        uid: Uid,
        flags: Option<MailFlags>,
        data: Vec<u8>,
        file_path: Option<PathBuf>,
    ) -> Result<Message> {
        let mut message = {
            let mail = parse_mail(&data)?;
            Message::headers_to_message(uid, flags, &mail.headers, file_path)?
        };
        message.data = data;
        Ok(message)
    }
    pub fn decrypted(envelope: &Message, data: Vec<u8>) -> Result<Message> {
        let mut envelope = envelope.clone();
        envelope.data = data;
        Ok(envelope)
    }
    pub fn header(&self) -> Vec<MailHeader> {
        // unwrap() is safe, we've already parsed the headers once
        parse_headers(&self.data).unwrap().0
    }
    pub fn get_alternative(&self, mimetype: &str) -> String {
        let email = parse_mail(&self.data).unwrap();
        let mut email = &email;
        if let Some(alternative) = find_part("multipart/alternative", email) {
            email = alternative;
        }
        let alternative = find_part(mimetype, email);
        if let Some(alternative) = alternative {
            alternative.get_body().ok().unwrap_or_default()
        } else {
            String::new()
        }
    }
}

fn find_part<'a>(mimetype: &str, mail: &'a ParsedMail<'a>) -> Option<&'a ParsedMail<'a>> {
    if mail.ctype.mimetype == mimetype {
        return Some(mail);
    }
    mail.subparts
        .iter()
        .find(|mail| mail.ctype.mimetype == mimetype)
}

pub fn get_header_or_none(headers: &[MailHeader], key: &str) -> Option<String> {
    headers.get_first_value(key)
}

fn get_header_or_empty(headers: &[MailHeader], key: &str) -> String {
    get_header_or_none(headers, key).unwrap_or_default()
}
