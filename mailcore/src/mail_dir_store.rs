use crate::errors::Result;
use crate::mail_flags::MailFlags;
use crate::mail_store::{Folder, MailStore, Name};
use crate::mail_store_state::MailStoreState;
use crate::message::{Message, Uid};
use crate::message_threads::MessageThreader;
use memchr::memchr;
use rayon::prelude::*;
use serde_derive::{Deserialize, Serialize};
use std::collections::HashMap;
use std::iter::FromIterator;
use std::path::{Path, PathBuf};
use std::sync::Arc;
use walkdir::{DirEntry, WalkDir};

#[derive(Deserialize, Debug, Serialize)]
pub struct MailDirConfig {
    path: PathBuf,
}

#[derive(PartialEq, Eq, Hash)]
struct Id(String, usize);

pub struct MailDirStore {
    path: PathBuf,
    map: HashMap<String, PathBuf>,
    path_map: Vec<PathBuf>,
}

impl MailDirStore {
    pub fn new(config: MailDirConfig) -> MailDirStore {
        MailDirStore {
            path: config.path,
            map: HashMap::new(),
            path_map: Vec::new(),
        }
    }
}

impl MailStore for MailDirStore {
    fn is_folder_complete(&self, folder: &str, state: &MailStoreState) -> bool {
        state.get_folder(folder).is_some()
    }
    fn list_folders(&mut self) -> Result<Vec<Folder>> {
        self.map.clear();
        Ok(list_folders(&self.path, &mut self.map))
    }
    fn fetch_headers(&mut self, folder: &str, threads: &mut MessageThreader) -> Result<()> {
        // get all messages
        if let Some(path) = self.map.get(folder) {
            let mut messages = get_messages(&self.path, path);
            messages.sort_by(|a, b| a.1.date.cmp(&b.1.date));
            for (path, mut m) in messages.into_iter() {
                m.uid = Uid(self.path_map.len());
                self.path_map.push(path);
                threads.add_message(Arc::new(m));
            }
        }
        Ok(())
    }
    fn fetch(&mut self, _folder: &str, uid: Uid) -> Result<Message> {
        read_message(uid, self.path.join(&self.path_map[uid.0]))
    }
}

fn is_mail_dir(entry: &DirEntry) -> bool {
    if !entry.file_type().is_dir() {
        return false;
    }
    let mut sub = entry.path().to_owned();
    for d in &["new", "cur", "tmp"] {
        sub.push(d);
        if !sub.exists() {
            return false;
        }
        sub.pop();
    }
    true
}

fn to_name(path: &Path) -> Option<Name> {
    let mut p = PathBuf::new();
    for path in path.iter() {
        if let Some(path) = path.to_str() {
            if path.starts_with('.') && path.ends_with(".directory") {
                let len = path.len();
                p.push(&path[1..len - 10]);
            } else {
                p.push(path);
            }
        } else {
            return None;
        }
    }
    p.to_str().map(|name| Name {
        name: name.to_string(),
        delimiter: "/".to_string(),
    })
}

fn list_folders(path: &Path, map: &mut HashMap<String, PathBuf>) -> Vec<Folder> {
    let walker = WalkDir::new(path);
    let mut folders = Vec::new();
    let prefix = path.iter().count();
    for entry in walker {
        if let Ok(entry) = entry {
            if is_mail_dir(&entry) {
                let relative = PathBuf::from_iter(entry.path().iter().skip(prefix));
                let name = to_name(&relative);
                if let Some(name) = name {
                    map.insert(name.name.clone(), relative);
                    folders.push(Folder {
                        name,
                        messages: None,
                        unread: None,
                    });
                }
            }
        }
    }
    folders.sort_by(|a, b| a.name.name.cmp(&b.name.name));
    folders
}

fn get_messages(base: &Path, relative: &Path) -> Vec<(PathBuf, Message)> {
    let mut path = base.join(relative);
    let mut paths = Vec::new();
    for (s, new) in &[("new", true), ("cur", false)] {
        path.push(s);
        if let Ok(it) = path.read_dir() {
            for e in it {
                if let Ok(e) = e {
                    if e.file_type().map(|ft| ft.is_file()).unwrap_or(false) {
                        paths.push((e.path(), *new));
                    }
                }
            }
        }
        path.pop();
    }
    let prefix = base.iter().count();
    let vecs: Vec<Vec<(PathBuf, Message)>> = paths
        .into_par_iter()
        .fold(
            || (Vec::new(), Vec::new()),
            |(mut buffer, mut result), (path, new)| {
                if let Ok(message) = read_headers(&mut buffer, path.clone(), new) {
                    let path = PathBuf::from_iter(path.iter().skip(prefix));
                    result.push((path, message))
                }
                (buffer, result)
            },
        )
        .map(|(_, r)| r)
        .collect();
    let mut r = Vec::new();
    for mut v in vecs.into_iter() {
        r.append(&mut v);
    }
    r
}

fn end_of_header(b: &[u8]) -> Option<usize> {
    if b.len() < 2 {
        return None;
    }
    let end = b.len() - 1;
    let mut start = 0;
    while start < end {
        if let Some(pos) = memchr(b'\n', &b[start..end]) {
            if b[start + pos + 1] == b'\n' {
                return Some(start + pos + 2);
            }
            start += pos + 2;
        } else {
            break;
        }
    }
    None
}

fn read_headers(content: &mut Vec<u8>, path: PathBuf, new: bool) -> Result<Message> {
    // read more and more until the message contains \n\n, end of headers
    use std::io::Read;
    let mut file = std::fs::File::open(&path)?;
    let mut size = 8192;
    content.resize(size, 0);
    let mut nread = file.read(content)?;
    let mut end = end_of_header(&content[0..nread]);
    while None == end {
        if nread != size {
            // read whole file, but did not find a complete header
            println!(
                "{} read {} bytes, but no end of headers found",
                path.display(),
                nread
            );
            return Err("File does not contain a complete header".into());
        }
        content.resize(2 * size, 0);
        nread += file.read(&mut content[size..])?;
        end = end_of_header(&content[size - 1..nread]);
        end = end.map(|end| end + size);
        size *= 2;
    }
    content.truncate(end.unwrap());
    let mut flags = MailFlags::from_file_name(path.file_name().unwrap());
    if flags.is_none() {
        flags = Some(MailFlags::from_seen(!new));
    }
    match Message::from_header_bytes(Uid(0), flags, &content, Some(path.clone())) {
        Err(e) => {
            println!("error parsing {}: {}", path.display(), e);
            Err(e)
        }
        Ok(o) => Ok(o),
    }
}

fn read_message(uid: Uid, path: PathBuf) -> Result<Message> {
    let content = std::fs::read(&path)?;
    Message::from_bytes(
        uid,
        MailFlags::from_file_name(path.file_name().unwrap()),
        content,
        Some(path),
    )
}
