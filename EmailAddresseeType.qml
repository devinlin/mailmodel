import QtQuick.Controls 1.4

ComboBox {
    property string addresseeType
    model: [ "To", "CC", "BCC" ]
    currentIndex: getIndex(addresseeType)
    onActivated: {
        if (index === 1) {
            addresseeType = "CC";
        } else if (index === 2) {
            addresseeType = "BCC";
        } else {
            addresseeType = "To";
        }
    }
    function getIndex(text) {
        if (text === "CC") {
            return 1;
        }
        if (text === "BCC") {
            return 2;
        }
        return 0;
    }
}
