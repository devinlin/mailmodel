use crate::interface::{FolderTreeListingEmitter, FolderTreeListingTrait, FolderTreeListingTree};
use mailcore::message::Message;
use mailcore::sorted_filtered_tree::SortedFilteredSyncedTree;
use mailcore::sync_tree::{TreeChange, TreeListener};
use mailcore::{FolderReceiver, Observer};
use std::cmp::Ordering;
use std::process::Command;
use std::sync::{Arc, Mutex};

#[derive(Clone)]
pub struct F(pub Arc<Mutex<FolderTreeListingData>>);

impl Observer for F {
    fn done(&self) -> bool {
        false
    }
}

impl FolderReceiver for F {
    fn add_changes(&mut self, changes: Vec<TreeChange<Arc<Message>>>) {
        if let Ok(mut s) = self.0.lock() {
            for change in changes {
                s.changes.push(change);
            }
            s.emit.new_data_ready(None);
        }
    }
}

pub struct FolderTreeListingData {
    emit: FolderTreeListingEmitter,
    changes: Vec<TreeChange<Arc<Message>>>,
}

pub struct FolderTreeListing {
    emit: FolderTreeListingEmitter,
    data: Arc<Mutex<FolderTreeListingData>>,
    tree: SortedFilteredSyncedTree<Arc<Message>, Listener>,
}

impl FolderTreeListing {
    pub fn data(&self) -> Arc<Mutex<FolderTreeListingData>> {
        self.data.clone()
    }
}

struct Listener {
    model: FolderTreeListingTree,
}

impl TreeListener for Listener {
    fn layout_about_to_be_changed(&mut self) {
        self.model.layout_about_to_be_changed()
    }
    fn layout_changed(&mut self) {
        self.model.layout_changed()
    }
    fn data_changed(&mut self, first: usize, last: usize) {
        self.model.data_changed(first, last)
    }
    fn begin_reset_model(&mut self) {
        println!("RESET MODEL");
        self.model.begin_reset_model()
    }
    fn end_reset_model(&mut self) {
        self.model.end_reset_model()
    }
    fn begin_insert_rows(&mut self, index: Option<usize>, first: usize, last: usize) {
        self.model.begin_insert_rows(index, first, last)
    }
    fn end_insert_rows(&mut self) {
        self.model.end_insert_rows()
    }
    fn begin_move_rows(
        &mut self,
        index: Option<usize>,
        first: usize,
        last: usize,
        dest: Option<usize>,
        destination: usize,
    ) {
        self.model
            .begin_move_rows(index, first, last, dest, destination)
    }
    fn end_move_rows(&mut self) {
        self.model.end_move_rows()
    }
    fn begin_remove_rows(&mut self, index: Option<usize>, first: usize, last: usize) {
        self.model.begin_remove_rows(index, first, last)
    }
    fn end_remove_rows(&mut self) {
        self.model.end_remove_rows()
    }
}

impl FolderTreeListingTrait for FolderTreeListing {
    fn new(mut emit: FolderTreeListingEmitter, model: FolderTreeListingTree) -> FolderTreeListing {
        let data = FolderTreeListingData {
            emit: emit.clone(),
            changes: Vec::new(),
        };
        let listener = Listener { model };
        let mut tree = SortedFilteredSyncedTree::new(listener);
        tree.set_sorter(Box::new(
            |depth: usize, a: &Arc<Message>, b: &Arc<Message>| {
                if depth > 0 {
                    return a.date.cmp(&b.date);
                }
                // sort by decreasing day order and by increasing time
                // that is how kmail does it by default
                match b.thread_end.date().cmp(&a.thread_end.date()) {
                    Ordering::Equal => a.thread_end.timestamp().cmp(&b.thread_end.timestamp()),
                    ord => ord,
                }
            },
        ));
        FolderTreeListing {
            emit,
            data: Arc::new(Mutex::new(data)),
            tree,
        }
    }
    fn emit(&mut self) -> &mut FolderTreeListingEmitter {
        &mut self.emit
    }
    fn row_count(&self, index: Option<usize>) -> usize {
        println!("row count {}", self.tree.row_count(index));
        self.tree.row_count(index)
    }
    fn index(&self, index: Option<usize>, row: usize) -> usize {
        self.tree.index(index, row)
    }
    fn parent(&self, index: usize) -> Option<usize> {
        self.tree.parent(index)
    }
    fn row(&self, index: usize) -> usize {
        self.tree.row(index)
    }
    fn check_row(&self, index: usize, row: usize) -> Option<usize> {
        self.tree.check_row(index, row)
    }
    fn uid(&self, index: usize) -> u64 {
        self.tree[index].uid.0 as u64
    }
    fn date(&self, index: usize) -> Option<String> {
        Some(format!(
            "{}",
            self.tree[index].as_ref().date.format("%y-%m-%d %H:%M")
        ))
    }
    fn section(&self, mut index: usize) -> String {
        // find the oldest date
        while let Some(parent) = self.parent(index) {
            index = parent;
        }
        let mut s = self.tree[index].as_ref().thread_end.to_rfc3339();
        s.truncate(10);
        s
    }
    fn from(&self, index: usize) -> Option<&str> {
        Some(self.tree[index].as_ref().from.as_str())
    }
    fn subject(&self, index: usize) -> Option<&str> {
        Some(self.tree[index].as_ref().subject.as_str())
    }
    fn seen(&self, index: usize) -> bool {
        self.tree[index]
            .flags
            .as_ref()
            .map(|f| f.seen)
            .unwrap_or(false)
    }
    fn file_path(&self, index: usize) -> Option<&str> {
        self.tree[index]
            .as_ref()
            .file_path
            .as_ref()
            .and_then(|p| p.to_str())
    }
    fn combined(&self, index: usize) -> String {
        let item = self.tree[index].as_ref();
        format!(
            "{}\0{}\0{}\0{}",
            item.subject,
            item.from,
            item.date,
            self.seen(index)
        )
    }
    fn can_fetch_more(&self, _index: Option<usize>) -> bool {
        !self.data.lock().unwrap().changes.is_empty()
    }
    fn fetch_more(&mut self, _index: Option<usize>) {
        let mut changes = Vec::new();
        if let Ok(mut l) = self.data.lock() {
            std::mem::swap(&mut changes, &mut l.changes);
        }
        self.tree.apply_all(changes);
    }
    fn set_filter(&mut self, filter: String) {
        use regex::{escape, RegexBuilder};
        let filter = filter.trim();
        if filter.is_empty() {
            self.tree.set_filter(Box::new(|_| true));
            return;
        }
        let regexes: Vec<_> = filter
            .split(' ')
            .map(|filter| {
                RegexBuilder::new(&escape(&filter))
                    .case_insensitive(true)
                    .build()
                    .unwrap()
            })
            .collect();
        self.tree.set_filter(Box::new(move |message| {
            regexes.iter().all(|regex| {
                regex.find(&message.subject).is_some() || regex.find(&message.from).is_some()
            })
        }));
    }
    fn open_mail_in_k_mail(&self, file_path: String) {
        std::thread::spawn(|| {
            if let Err(e) = Command::new("kmail").arg("--view").arg(file_path).status() {
                eprintln!("{:?}", e);
            }
        });
    }
}
