# Simple mail  viewer using Rust Qt Binding Generator

## Compilation

Install cmake, rustc, cargo, Qt, and make or ninja.

```bash
cd mailmodel
mkdir build
cd build
cmake -GNinja ..
ninja
```

## Running the progam

Create a configuration file for MailDir or IMAP.

```json
{
    "MailDir": {
        "path": "/home/user/mail"
    }
}
```

```json
{
    "IMAP": {
        "domain": "imap.electronmail.org",
        "username": "username",
        "password": "v3ry53cur3",
        "port": 993,
        "ssl": true
    }
}
```

```bash
./mailmodel config.json
```

## Development tip

### Changing the bindings

If you want to change `bindings.json`, Rust Qt Binding Generator should be installed.

### Testing with IMAP

To try mail commands on the command-line connect to an IMAP server like so:
  openssl s_client -connect example.com:993
